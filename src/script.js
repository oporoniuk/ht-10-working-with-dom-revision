// Теоретичні питання
// 1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
//document.createElement('tag'); document.createTextNode('text')

// 2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

// Стаорюємо змінну, яка буде дорівнювати тегу з класом 'navigation'. Знаходимо цей тег за допомогою querySelector.
//Щоб видалити ми викликаємо метод remove() нашого елемента, я зробила це через click , щоб було наочно видно як зникає елемент.
const navigation = document.querySelector(".navigation");
console.log(navigation);
navigation.onclick = function(event) {
navigation.remove();
};



// 3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

//before(); after();
//insertAdjacentHTML('beforebegin', html); insertAdjacentHTML('afterend', html)



// Практичні завдання
// 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". 
//Додайте цей елемент в footer після параграфу.

const a = document.createElement('a');
a.href = '#';
console.log(a.href);
a.textContent = "Learn More";
console.log(a);
document.getElementById('footer-p').after(a);



// 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", 
//і додайте його в тег main перед секцією "Features".

document.querySelector('.features').insertAdjacentHTML("beforebegin", '<select id="rating"></select>');


// Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", 
//і додайте його до списку вибору рейтингу.

// Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", 
//і додайте його до списку вибору рейтингу.


// Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", 
//і додайте його до списку вибору рейтингу.


// Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", 
//і додайте його до списку вибору рейтингу.
for (let a = 4; a > 0; a--) {  
    const option = document.createElement('option');     
    option.value = a;
    option.textContent = a +' Star';
    console.log(option);
    document.getElementById('rating').append(option);
};




